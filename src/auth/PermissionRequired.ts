import type { KeycloakInterface } from '@auth/keycloak';
import { KeycloakStore } from '@auth/keycloak';
import { goto, beforeNavigate } from '$app/navigation';
import { EventRole } from './models';
import type { Unsubscriber } from 'svelte/types/runtime/store';

let keycloak: KeycloakInterface | undefined = undefined;
KeycloakStore.subscribe((data) => {
  keycloak = data.keycloak;
});

export function redirectToLogin(): Promise<void> {
  return goto(`/login?referer=${window.location.pathname}`);
}

export function redirectToUnauthorized(): Promise<void> {
  return goto(`/error/unauthorized?referer=${window.location.pathname}`);
}

let KeycloakAuthRequiredSubscription: Unsubscriber | null = null;
export async function requireAuth() {
  if (!keycloak?.authenticated) await redirectToLogin();

  if (!KeycloakAuthRequiredSubscription) {
    // If authenticated, we need to keep it that way
    // Subscribe to updates for the purpose of redirecting to login page
    KeycloakAuthRequiredSubscription = KeycloakStore.subscribe(async (data) => {
      if (!data.keycloak?.authenticated) await redirectToLogin();
    });

    // The next page might not require auth, so stop this subscription
    beforeNavigate(() => {
      if (!KeycloakAuthRequiredSubscription) return;

      KeycloakAuthRequiredSubscription();
      KeycloakAuthRequiredSubscription = null;
    });
  }
}

async function requireRole(
  requiredRole: EventRole,
  keycloakInstance: KeycloakInterface | undefined = keycloak,
): Promise<void> {
  // First of all, auth is required (and requireAuth watches for changes to auth)
  await requireAuth();
  // Then check for role
  if (!keycloakInstance?.getFirstRole([requiredRole, EventRole.SuperAdmin])) {
    await redirectToUnauthorized();
  }
}

export default requireRole;
