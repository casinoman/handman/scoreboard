import type { AxiosResponse, Method } from 'axios';
import axios from 'axios';
import ENV from '../env';
import { KeycloakInterface, KeycloakStore } from '@auth/keycloak';
import { EventRole } from '@auth/models';
import type { NullableEventRole } from '@auth/models';
import type { ProcessedFile } from '@models/Storage';

type StorageApiHeaders = {
  Authorization?: string;
  'x-hasura-role'?: EventRole;
  'Content-Type'?: string;
  'Bucket-Id'?: string;
};

export type PreSignedUrlResponse = {
  url: string;
  expiration: number;
};

export type StorageApiResponse = {
  processedFiles: ProcessedFile[];
};

let keycloak: KeycloakInterface | undefined;
KeycloakStore.subscribe((value) => (keycloak = value.keycloak));

const axiosAPI = axios.create({
  baseURL: ENV.StorageApi,
});

function getHeaders(role: NullableEventRole): StorageApiHeaders {
  const headers: StorageApiHeaders = {
    Authorization: `Bearer ${keycloak?.token}`,
  };
  if (role) headers['x-hasura-role'] = role;
  return headers;
}

export async function storageApiRequest<T>(
  method: Method,
  url: string,
  data?: string | FormData | Record<string, unknown>,
): Promise<AxiosResponse<T>> {
  const headers = getHeaders(keycloak?.getFirstRole([EventRole.SuperAdmin, EventRole.CTFAdmin, EventRole.Participant]));
  return axiosAPI({ method, url, data, headers });
}

export async function getStorageFilePreSignedUrl(fileId: string): Promise<string> {
  const response = await storageApiRequest<PreSignedUrlResponse>('GET', `/v1/files/${fileId}/presignedurl`);
  // We might know the server's "external address" better than it does
  // so replace the server's base URL with our version of it
  const serverUrl = new URL(response.data.url);
  const url = new URL(serverUrl.pathname, ENV.StorageApi);
  url.search = serverUrl.search;
  return url.href;
}

export function postFiles<T = StorageApiResponse>(files: FileList): Promise<AxiosResponse<T>> {
  const data = new FormData();
  for (const file of files) {
    data.append('metadata[]', new Blob([JSON.stringify({ name: file.name })], { type: 'application/json' }));
    data.append('file[]', file);
  }

  const headers = getHeaders(keycloak?.getHighestPrivilegeRole());
  return axiosAPI({ method: 'POST', url: '/v1/files/', headers, data });
}
