import { KeycloakInterface } from '@auth/keycloak';
import { Gql } from '../db';

// disable server side rendering on all pages,
// the keycloak library doesn't support it well
export const ssr = false;

/** @type {import('./$types').LayoutLoad} */
export async function load() {
  // Initialize sets up the Keycloak Sink in the GQL class
  Gql.initialize();
  // Initialize Keycloak
  const keycloak = new KeycloakInterface();
  await keycloak.setupKeycloak();
  // Make keycloak available to +page files
  return { keycloak };
}
