/* eslint-disable */
import { writable } from 'svelte/store';
import type { Writable } from 'svelte/store';
import { KeycloakData } from '@auth/keycloak';

export const Keycloak: Writable<KeycloakData> = writable(new KeycloakData());

export function getHighestPrivilegeRole() {
  return 'admin';
}

export const KeycloakInterface = jest.fn();
