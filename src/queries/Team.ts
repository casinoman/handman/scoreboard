import type { Team } from '@models/Team';
import { Gql } from '../db';
import { KeycloakInterface, KeycloakStore } from '@auth/keycloak';

let keycloak: KeycloakInterface | undefined;

KeycloakStore.subscribe((value) => {
  keycloak = value.keycloak;
});

export async function GetBlueTeams(): Promise<Array<Team>> {
  const q = `query blueteams @cached(ttl: 300) {
        blueteam (order_by: {id: asc}) {
            id
            name
        }
    }`;
  // Get the highest privileged role this user has
  const queryRole = keycloak?.getHighestPrivilegeRole();
  return ((await Gql.query(q, { role: queryRole })) as { blueteam: Array<Team> }).blueteam;
}

export async function GetAllEnabledBlueTeams(): Promise<Array<Team>> {
  let teams: Array<Team> = [];
  const q = `query scoreboard_team_settings @cached(ttl: 300) {
    scoreboard_team_settings(order_by: {team: {name: asc}}) {
      team {
        id
        name
      }
    }
  }`;
  // Get the highest privileged role this user has
  const queryRole = keycloak?.getHighestPrivilegeRole();

  await Gql.query(q, { role: queryRole })
    .then((res) => {
      teams = (res as { scoreboard_team_settings: Array<{ team: Team }> }).scoreboard_team_settings.map((e) => {
        return e.team;
      });
    })
    .catch((err) => {
      throw err;
    });
  return teams;
}

export async function getScoreboardTeams() {
  /*
   * Retrieve teams for displaying on the scoreboard. We allow:
   * - admin TEAM roles (not explicitly admin scoreboard role) to view all hidcen teams
   * - Members of a hidden team to view hidden teams
   *
   * Note that teams aren't prevented from retrieving the data about hidden teams;
   * This just allows us to hide them from the ui under normal circumstances.
   */
  try {
    return await GetAllEnabledBlueTeams();
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error('error setting up teams', err);
    return Array<Team>();
  }
}
