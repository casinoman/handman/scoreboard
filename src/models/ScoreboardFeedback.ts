export type ScoreboardFeedback = {
  team_id: string;
  team_name: string;
  id: number;
  serviceName: string;
  service_id: number;
  event_result: number;
  extended_data: number;
  participant: JSON;
  staff: JSON;
};

export type ScoreboardFeedbackResults = {
  scoreboard_feedback: Array<ScoreboardFeedback>;
};
