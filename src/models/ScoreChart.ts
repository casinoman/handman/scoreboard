import type { Team } from './Team';

export type Scores = {
  ctf: string;
  bonus: string;
  service: string;
  total: string;
  team: Partial<Team>;
};

export type ScoreResponse = {
  scores: Array<Scores>;
};

export function ScoresQuery(): string {
  return `{
    scoreboard_score(order_by: {team: {name: asc}}) {
      service
      event_id
      total
      team_id
      team {
        id
        name
      }
      ctf
      bonus
    }
  }`;
}
