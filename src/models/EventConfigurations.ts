export type EventConfiguration = {
  id: string;
  name: string;
  start: string | null;
  end: string | null;
};

export type EventConfigurationReturn = {
  events: Array<EventConfiguration>;
};

export const AllEventCfgQuery = `{
  events(order_by: {start: asc}) {
    end
    id
    name
    start
  }
}`;
