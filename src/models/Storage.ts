export type ProcessedFile = {
  id: string; // UUID
  name: string;
  size: number;
  bucketId: string;
  etag: string;
  createdAt: string; // DATE
  updatedAt: string; // DATE
  isUploaded: boolean;
  mimeType: string;
  uploadedByUserId: string;
};

export type StorageBucket = {
  id?: string;
  createdAt?: string;
  updatedAt?: string;
  downloadExpiration?: number;
  minUploadFileSize?: number;
  maxUploadFileSize?: number;
  cacheControl?: string;
  presignedUrlsEnabled?: true;
  files?: StorageFile[];
};

export type StorageFile = {
  id?: string;
  createdAt?: string;
  updatedAt?: string;
  bucketId?: string;
  name?: string;
  size?: string;
  mimeType?: string;
  etag?: string;
  isUploaded?: boolean;
  uploadedByUserId?: string;
  bucket?: StorageBucket;
};
