import { Gql } from '../db';
import type { Team } from '@models/Team';

export type Service = {
  id: number;
  name: string;
  display_name: string;
  description: string;
};

export type ServiceStatus = {
  check_id: number;
  check_time: string;
  result_code: string;
  visible: boolean;
  event_id: string;
  participant: Participant | null;
  check: Check;
};

export type Check = {
  display_name: string;
  description: string;
  id: string;
  name: string;
};
export type Participant = {
  feedback: string;
  detailed_results: Array<string>;
};
export type ScoreBoardServiceStatus = {
  event_id: string;
  team: Team;
  check_time: string;
  result_code: string;
  visible: boolean;
  participant: Participant | null;
  check: Check;
};

export const ServiceStatusQuery = `ServiceStatus {
  scoreboard_service_status(order_by: {team: {name: asc}, check_id: asc}) {
    event_id
    team {
      id
      name
    }
    check {
      id
      name
      display_name
      description
    }
    visible
    participant
    result_code
    check_time
  }
}`;

export async function GetServiceStatus(): Promise<Array<ScoreBoardServiceStatus>> {
  let services: ScoreBoardServiceStatus[] = [];

  const q = `query ${ServiceStatusQuery}`;

  await Gql.query(q)
    .then((res) => {
      services = (res as { scoreboard_service_status: Array<ScoreBoardServiceStatus> }).scoreboard_service_status;
    })
    .catch((err) => {
      throw err;
    });
  return services;
}

export async function GetServiceNames() {
  let ret: Partial<Check>[] = [];
  const q = `query ServiceStatus {
  scoreboard_displayed_services {
    check {
      display_name
    }
  }
}`;
  await Gql.query(q)
    .then((res) => {
      ret = (
        res as { scoreboard_displayed_services: Array<{ check: Partial<Check> }> }
      ).scoreboard_displayed_services.map((e) => {
        return e.check;
      });
    })
    .catch((err) => {
      throw err;
    });
  return ret;
}
