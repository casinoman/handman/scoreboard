import { Gql } from '../db';
import type { EventRole } from '@auth/models';

export type Team = {
  id: string;
  name: string;
};

export type TeamResponse = {
  teams: Array<Team>;
};

/**
 * Retrieve Team from Database
 * @param teamId UUID of Team to Retrieve
 * @param role Optionally provide a role to perform the query with. The Staff role will grant access to hidden teams.
 */
export async function TeamById(teamId: string, role: EventRole | null = null): Promise<Team> {
  const teamByIdQuery = `query team_by_id {
        teams(where: {id: {_eq: ${JSON.stringify(teamId)}}}) {
            id
            name
        }
    }`;

  try {
    const values = ((await Gql.query(teamByIdQuery, { role })) as TeamResponse).teams;
    if (values.length < 1) {
      throw `no team found with id '${teamId}'`;
    }
    return values[0];
  } catch (error) {
    const msg = `error retrieving team id '${teamId}'`;
    console.error(msg, error);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    throw new Error(msg, { cause: error });
  }
}
