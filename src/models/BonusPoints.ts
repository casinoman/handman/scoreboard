import type { Team } from './Team';

export type BonusPoints = {
  id: number;
  team_id: number;
  points: number;
  reason?: string;
  created_at: Date;
  team?: Team;
};

export type BonusPointsReturn = {
  other_points: Array<BonusPoints>;
};

export const BonusPointsQuery = `{
    other_points(order_by: {id: asc}) {
        id
        team_id
        points
        reason
        created_at
    }
}`;

export const BonusPointsWithTeamQuery = `{
    other_points(order_by: {id: asc}) {
        id
        team_id
        points
        reason
        created_at
        team {
            id
            name
            role_name
            disabled
        }
    }
}`;
