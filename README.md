# Cyboard

**Cyboard** is a scoring engine for the cyber defense competition
[CNY Hackathon](https://www.cnyhackathon.org 'CNY Hackathon Home').

<!-- Generated with https://github.com/jonschlinkert/markdown-toc -->
<!-- toc -->

- [Cyboard](#cyboard)
  - [Background](#background)
  - [Features](#features)
    - [Web Application Server](#web-application-server)
  - [Building](#building)
  - [Development](#development)
  - [Administration](#administration)
    - [CTF File Uploads](#ctf-file-uploads)
    - [Users and Roles](#users-and-roles)

<!-- tocstop -->

## Background

CNY Hackathon is a joint cyber security defense & CTF event for intercollegiate
students in the US North East region. The event is hosted bi-annually to
100+ contestants. Despite the name, it shares no similarities with a
[programming hackathon](https://en.wikipedia.org/wiki/Hackathon).

[Tony](https://github.com/pereztr5 'Tony Perez') first developed Cyboard in 2016
as his senior project at SUNY Polytechnic. Ever since Tony's graduation,
[Butters](https://github.com/tbutts 'Tyler Butters') stepped up as the project's
developer.
TODO: Some notes about the updates since CAE

## Features

## Building

Use Docker to build the production image:

```bash
docker build .
```

## Development

### Editor

Recommended editors are [Webstorm](https://www.jetbrains.com/webstorm/) or [VS Code](https://code.visualstudio.com/).

Whatever editor you choose, please configure it to:

- Automatically apply [prettier](https://prettier.io/) formatting on save
- Support Typescript, Javascript, and Svelte
- Display ESLint errors _based on the project linting configuration_

### Running the App

There are two ways to run the scoreboard UI - Via Docker or locally using npm.

#### Common Steps

Before firing up the scoreboard application:

1. Start Hasura from it's dedicated repository: https://gitlab.com/c2-games/scoring/hasura
   - This will start Hasura on the `c2games` Docker Network, and expose Hasura on http://localhost:8080
   - Note: If using a cloud-based Hasura instance, skip this step
2. If running in Sandbox mode, start the Sandbox API server.
   - If you're unsure if you need sandbox mode, you don't :)
3. Configure your environment with the .env file
   - The following options are available:
     - VITE_GQL_PUBLIC_URI: URI of GraphQL, usually localhost:8080. This is overridden by docker-compose when using docker
     - VITE_SANDBOX_API_URI: URI of the Sandbox API. This is only needed if running in Sandbox Mode.
     - VITE_ASSET_STORAGE_URI: URI of Static Asset Storage.
     - VITE_KEYCLOAK_URL: URI of keycloak - usually https://auth.c2games.org
     - VITE_KEYCLOAK_REALM: Realm of Keycloak - usually `dev` for development.
     - VITE_KEYCLOAK_CLIENT_ID: Client ID within keycloak to utilize. Usually `scoreboard-ui`
     - VITE_EVENT_ID: Event that this scoreboard instance is displaying data for. Does not generally affect staff roles.
     - VITE_STORAGE_API: API URL for Hasura Storage Service. Must be base URL, /v1/\* will be appended.
   - Example .env file:
   ```dotenv
   VITE_GQL_PUBLIC_URI=http://localhost:8080/v1/graphql
   VITE_SANDBOX_API_URI=http://127.0.0.1:8000/
   VITE_ASSET_STORAGE_URI=
   VITE_KEYCLOAK_URL=https://auth.c2games.org
   VITE_KEYCLOAK_REALM=dev
   VITE_KEYCLOAK_CLIENT_ID=scoreboard-ui
   VITE_EVENT_ID=7803635d-419f-420d-a426-0f27b8c1e08b
   VITE_STORAGE_API=http://localhost:8181
   ```

#### Running UI with Docker

The first time you run docker, you will need to execute the script
`setup/nginx/generate_localhost_ssl_certs.sh` to generate SSL keys.

To start the application with docker, simple run `docker-compose up` and access the app on `https://localhost`.

#### Running UI locally

The application can also be run locally using npm:

```bash
# Install depedencies
npm install
# Run the development server
npm run dev
```

### Redeploying after changes

Changes to the UI should be automatically reloaded.

### Code Quality Control

Currently, two tools are in use for code quality: Prettier and ESLint. Both are expected to be passing for all contributions.

To run both tools, use the command `npm run lint`

To attempt any automatic fixes available, such as applying prettier formatting or eslint auto-fixes, use the command:

```bash
npm run lint:fix
```

#### ESLint

To run only ESLint checks, use the command `npm run lint:eslint`.

#### Prettier

To run only Prettier checks, use the command `npm run lint:format`.

## Administration

### CTF File Uploads

TODO: Talk about how the `data` directory is stored on a docker volume for persistence.

### Users and Roles

TODO: Need to figure out how best to do keycloak still
