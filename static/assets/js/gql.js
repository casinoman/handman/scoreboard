if(!window.C2GamesScoreboard){
    window.C2GamesScoreboard = {}
}
if(!window.C2GamesScoreboard.db){
    window.C2GamesScoreboard.db = {}
}

window.C2GamesScoreboard.objToGqlStr = (obj) => {
    ret = ""
    Object.keys(obj).map(function(objectKey, index) {
        ret += `${objectKey}: ${JSON.stringify(obj[objectKey])} `
    });
    return ret
}
