/*
 * Contains the models for queries the anon role can perform. For admin queries,
 * models will be created on the appropriate admin page with the additional fields defined.
 */
(() => {
    if(!window.C2GamesScoreboard){
        window.C2GamesScoreboard = {}
    }
    if(!window.C2GamesScoreboard.models){
        window.C2GamesScoreboard.models = {}
    }
    let exports = window.C2GamesScoreboard.models;

    class GQLQuerySubscribe {
        async query(...args) {
            /*
             * Make a one-time query
             * payloadOptions (object): See C2GamesScoreboard.db.subscribe for payload options.
             */
            return C2GamesScoreboard.db.query(`query ${this.getQuery}`, ...args)
        }

        subscribe(...args) {
            /*
             * Subscribe to changes to the database using a sink interface
             * sink (object): Sink Interface containing callbacks for complete, error and next events
             * https://github.com/enisdenjo/graphql-ws/blob/master/docs/interfaces/common.Sink.md
             * payloadOptions (object): See C2GamesScoreboard.db.subscribe for payload options.
             */
            return C2GamesScoreboard.db.subscribe(`subscription ${this.getQuery}`, ...args);
        }

        get getQuery() {
            return '{}'
        }
    }

    class C2GamesGqlModel extends GQLQuerySubscribe {
        /*
         * Base class used for generic querying and subscribing to GraphQL
         */
        constructor(name, fields, {parameters, filter} = {}) {
            super();
            // Name of table in database
            this.name = name;
            // Fields to include in a query
            this.fields = fields;
            // parameters, declared as Hasura expects them. Ex, ($where: scores_bool_exp)
            this._parameters = parameters || '';
            // Top-level filter
            this._filter = filter || '';
        }

        async query(...args) {
            // todo do we rethink this to take ...args as parameters to the query?
            // ensure the fragment is registered
            return super.query(...args);
        }

        subscribe(...args) {
            // ensure the fragment is registered
            return super.subscribe(...args);
        }

        getFields({include, exclude} = {include: [], exclude: []}) {
            return this.fields.concat(include || []).filter(x => !exclude.includes(x))
        }


        get getQuery() {
            return `${this._parameters}{ ${this.name}${this._filter} { ${this.getFields().join(' ')} }}`
        }
    }

    /*
     * Model definitions for common, simple queries
     */
    exports.ServiceScores = new C2GamesGqlModel('service_score', ['team_id', 'points']);
    exports.Challenges = new C2GamesGqlModel('ctf_challenges', ['id', 'name', 'description', 'category', 'points']);
    exports.ChallengesAll = new C2GamesGqlModel('ctf_challenges', [
        'id', 'designer', 'description', 'category',
        'flag', 'is_hidden', 'name', 'points'
    ]);
    exports.ChallengeSolves = new C2GamesGqlModel('challenge_solves', [
        'id', 'challenge_id', 'event_id', 'solved_by',
        'team {name disabled}', 'challenge {id name points}'
    ]);
    exports.ServiceStatus = new C2GamesGqlModel('current_service_status', [
        'service_id', 'service_name', 'event_result', 'team_id', 'team_name', 'participant'
    ]);

    class Scores extends C2GamesGqlModel {
        /*
         * POC for supporting getting all scores, or getting scores by team id
         */
        _getVars(teamId) {
            return {"where": teamId ? {"team": {"id":{"_eq": teamId}}} : null}
        }

        async query(teamId = null) {
            return super.query({variables: this._getVars(teamId)})
        }

        subscribe(sink, teamId = null) {
            return super.subscribe(sink, {variables: this._getVars(teamId)});
        }
    }
    exports.Scores = new Scores(
        'scores',
        ['ctf_score', 'other_score', 'service_score', 'total_score', 'team {id, name}'],
        {
            parameters: '($where: scores_bool_exp)',
            filter: '(where: $where)'
        }
    );

    /*
     * For reusable complex queries, use a model that extends GQLQuerySubscribe and override getQuery
     */
    // this actually requires admin privs /shrug
    class ServiceDetails extends GQLQuerySubscribe {
        async query(name, disabled = false, {variables, ...args} = {}) {
            variables = variables || {}
            return super.query({
                variables: {...variables, name, disabled},
                ...args
            });
        }

        async subscribe(sink, name, disabled = false) {
            throw new Error("This model selects Multiple top-level Objects and cannot be subscribed to")
            // return super.subscribe(sink, {variables: {name}});
        }

        get getQuery() {
            return `($name: String!, $disabled: Boolean!){
              service(
                where: { name: { _eq: $name }}
              ) {
                id
                name
                category
                description
                total
                points
                script
                args
                disabled
                starts_at
                created_at
                modified_at
                api_uri
                interval
                min_rand
                max_rand
                last_octet
                send_to_api
                self_destruct
                self_destruct_min_rand
                self_destruct_max_rand
              }
              team(where: {disabled: {_eq: $disabled}, _and: {role_name: {_eq: blueteam}}}) {
                id
                name
                internal_subnet
                external_ip
              }
            }`
        }
    }
    exports.ServiceDetails = new ServiceDetails()
})();