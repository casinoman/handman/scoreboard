// Global DOM references to the teams table and editor modal.
// The modal gets reused for both adding a single new team and editing an existing one.
const $modal = $('#team-edit-modal');
const $cfgTable = $('.team-config-table');

// isBlueteam helps determine if the external_ip and internal_subnet form/json field is necessary
// (when displaying the modal or submitting JSON data).
const isBlueteam = (role_name) => role_name === "blueteam";

/* Show modal editor */
$cfgTable.on('click', '.btn-edit', function showTeamEditorModal(event) {
    const $row = $(event.currentTarget).parentsUntil('tr').parent()

    // Very tight DOM coupling
    const $form = $modal.find('form');
    const findInput = (name) => $form.find(`input[name=${name}]`);

    const $cells = $row.children();
    const cellText = (idx) => $cells.eq(idx).text();

    $modal.find('.modal-title').text(`Edit "${cellText(1)}"`);
    $modal.find('input[name=newteam]').val("false");

    // Plug in the role name to the combo box, and fire a change event to enable/disable
    // the external_ip field, if editing a blueteam.
    const role_name = cellText(2);
    $form.find("select[name=role_name]").val(role_name).trigger('change');

    findInput("id").val(cellText(0));
    findInput("name").val(cellText(1));
    findInput("external_ip").val(cellText(3));
    findInput("internal_subnet").val(cellText(4));

    // The icon is only displayed when disabled
    const isDisabled = $cells.eq(5).children().length > 0;
    findInput("isdisabled").prop('checked', isDisabled);
    // The icon is only displayed when not visible
    const not_visible = $cells.eq(6).children().length > 0;
    findInput("visible").prop('checked', !not_visible);
    // The icon is only displayed when early login is disabled
    const not_early_login = $cells.eq(7).children().length > 0;
    findInput("early_login").prop('checked', !not_early_login);

    $modal.modal('show');
});

/* Disable blueteam_ip field based on selected role_name */
$modal.find('form').on('change', 'select[name=role_name]', function(event) {
    const $form = $(event.delegateTarget);
    const role_name = $(event.currentTarget).val();

    const enable = isBlueteam(role_name);
    $form.find("input[name=external_ip]")
        .prop("required", enable)
        .parent(".form-group").toggle(enable);
    $form.find("input[name=internal_subnet]")
        .prop("required", enable)
        .parent(".form-group").toggle(enable);
});

/* When showing the modal, hide the "Delete" button if the modal is being used to create. */
$modal.on('show.bs.modal', function(event) {
    const isNewTeam = $(event.relatedTarget).hasClass("btn-add-team");
    $modal.find('form').find('.delete-team').toggle(!isNewTeam);
});


/* Parse the modal form into either a TeamModRequest for the server. */
function teamFormAsJson($form) {
    const findInput = (name) => $form.find(`input[name=${name}], select[name=${name}]`);
    const strInput = (name) => findInput(name).val();
    const intInput = (name) => parseInt(findInput(name).val(), 10);
    const boolInput = (name) => findInput(name).val() == "true";

    // Parse form data into JSON
    const data = {
        new_team: boolInput("newteam"),
        id: intInput("id"),
        name: strInput("name"),
        role_name: strInput("role_name"),
        disabled: findInput("isdisabled").prop("checked"),
        visible: findInput("visible").prop("checked"),
        early_login: findInput("early_login").prop("checked"),
    };

    // Blueteams have two extra fields, for their internal and external IP address/network.
    if(isBlueteam(data.role_name)) {
        data.external_ip = strInput("external_ip");
        data.internal_subnet = strInput("internal_subnet")
    }

    return data;
}

/* Submit new/editted Team */
$modal.find('form').on('submit', function saveTeam(event) {
    event.preventDefault();
    const $form = $(this);
    const data = teamFormAsJson($form);

    // Hasura's handling doesn't allow this field to be quoted
    // because it is an enum not a string
    // we insert it back in special
    let team_role_kv = `role_name: ${data.role_name}`
    delete data.role_name;

    const isNewTeam = data.new_team;
    delete data.new_team;
    let gql_fields = window.C2GamesScoreboard.objToGqlStr(data);
    // insert the team_role
    gql_fields = gql_fields.slice(0, -1) + "," + team_role_kv;
    // insert mutation if new team, otherwise update
    let mutation = ""
    if(isNewTeam) {
        mutation = `insert_team_one(object:{${gql_fields}}) {id}`;
    } else {
        mutation = `update_team(where: {id: {_eq: ${data.id}}}, _set:{${gql_fields}}){ affected_rows returning {id} }`;
    }
    C2GamesScoreboard.db.query(`mutation team { ${mutation} }`, {role: 'admin'}).then(function(ret) {
        console.log(`QGL ret: ${ret}`);
        alert("Page will reload.");
        window.location.reload();
    }).catch(e => {
        console.error(`error in team submit: ${JSON.stringify(e)}`);
        alert(`Error occurred on submit: ${JSON.stringify(e)}`);
    });
});

/* Delete Team */
$modal.find('form').on('click', '.delete-team', function deleteTeam(event) {
    const $form = $(event.delegateTarget);
    const id = $form.find("input[name=id]").val();
    const name = $form.find("input[name=name]").val();

    if(confirm(`Are you sure you want to delete "${name}"`)) {
        let mutation = `delete_team(where: {id: {_eq: ${id}}}) { affected_rows returning {id} }`
        C2GamesScoreboard.db.query(`mutation delete_team { ${mutation} }`, {role: 'admin'}).then(function(ret) {
            console.log(`QGL ret: ${JSON.stringify(ret)}`);
            alert("Page will reload.");
            window.location.reload();
        }).catch(e => {
            console.error(`error in team submit`);
            alert(`Error occurred on submit`);
        });
    };
});


/* Add new team, button below the table */
$('.btn-add-team').on('click', function showTeamAddModal(event) {
    const $form = $modal.find('form');
    $form.trigger('reset');
    $form.find("select[name=role_name]").trigger("change");

    $modal.find('.modal-title').text("Add new team");
    $modal.find('input[name=id]').val("");
    $modal.find('input[name=newteam]').val("true");

    // Modal show is wired up in the HTML, which then exposes the "relatedTarget"
    // on the event (part of bootstrap's API), which is used to hide/show a Delete button.
    //$modal.modal('show');
});


/* Upload Blue teams via CSV */
const $csvUploadForm = $('.csv-upload form');

(function initTeamsCsvEditor() {
    const placeholderCsv = `
                    Name, IP, Password
             red jaguars,  1, smokestack
          91st tech unit,  2, bologna
            joel spolsky,  3, xmas_monkEE
"a single sleepy, doxin",  4, d2hhdCB0aGUgZnVjayBkaWQgeW91IGV4cGVjdAo=
`.slice(1, -1); // trim the newlines at the start and end (but not the spaces)

    $csvUploadForm.find('textarea').val(placeholderCsv);
})();

$csvUploadForm.on('submit', function addTeamsViaCSV(event) {
    event.preventDefault();
    const rawCSV = $csvUploadForm.find('textarea').val();

    let data;
    try {
        data = $.csv.toObjects(rawCSV, { onParseValue: e => e.trimLeft() });
    } catch (e) {
        alert(`CSV Upload failed: ${e.message}`)
        throw e;
    }

    // Lowercase all keys, cast values to scalars, rename fields as needed
    data = data.map((row, idx) => {
        row = Object.assign(...Object.entries(row).map(([k, v]) => ({[k.toLowerCase()]: v })));

        try {
            row.blueteam_ip = parseInt(row.ip, 10);
            delete row.ip;
        } catch(e) {
            alert(`row ${idx}: parsing IP as int failed: ${e.message}`);
            throw e;
        }

        delete row.id;
        return row;
    });

    // This request can take a while, because each team's password has to be hashed,
    // so a spinner will be displayed to show some form of progress
    const loading = toggleLoadingButton($(this).find('button[type=submit]'));
    loading(true);

    let mutation = `insert_team(objects:{${window.C2GamesScoreboard.objToGqlStr(data)}}) { affected_rows returning {id} }`
    C2GamesScoreboard.db.query(`mutation team_insert_multiple { ${mutation} }`, {role: 'admin'}).then(function(ret) {
        console.log(`QGL ret: ${ret}`);
        alert("Page will reload.");
        window.location.reload();
    }).catch(e => {
        console.error(`error in team submit: ${e}`);
        alert(`Error occurred on submit: ${e}`);
    }).always(() => {
        loading(false);
    });
});

