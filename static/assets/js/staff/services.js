/* NOTE: This code is very similar to teams.js, and could be refactored. */

// Global DOM references to the teams table and editor modal.
const $modal = $('#service-edit-modal');
const $cfgTable = $('.services-config-table');

// pad any date component with a leading 0. e.g. Sept -> 09
function pad(datePart) {
    return ("0" + datePart).slice(-2);
}

/* Show modal editor */
$cfgTable.on('click', '.btn-edit', function showServiceEditorModal(event) {
    const $row = $(event.currentTarget).parentsUntil('tr').parent()
    const id = $row.data('service-id');

    const $form = $modal.find('form');
    const findInput = (name) => $form.find(`input[name=${name}]`);

    // Query the service api directly to get the data for this service in JSON
    const url = `/api/admin/services/${id}`
    $.getJSON(url).done(srv => {
        // Set a bunch of form fields from the JSON
        ["id","name","display_name","category","description","points",
         "script","last_octet","interval","min_rand","max_rand"].forEach(k => {
            findInput(k).val(srv[k]);
        });

        // Quote all args because there's only one text input and this is
        // the safest way to maintain the hacky arg parsing.
        //findInput("args").val(srv.args.map(s => `"${s}"`).join(" "));
        findInput("args").val(srv.args);
        findInput("disabled").prop('checked', srv.disabled);
        findInput("scorable").prop('checked', srv.scorable);

        // Decompose start time into two separate inputs, because between
        // browsers this is the most supported way to create a date+time picker.
        const dt = new Date(srv.starts_at);
        findInput("starts_at_date").val(
            `${dt.getFullYear()}-${pad(dt.getMonth()+1)}-${pad(dt.getDate())}`);
        findInput("starts_at_time").val(
            `${pad(dt.getHours())}:${pad(dt.getMinutes())}`);

        $modal.find('.modal-title').text(`Edit "${srv.name}"`);

        $modal.modal('show');
    }).fail(xhr => {
        alert(`Failed to get service "${id}": ${getXhrErr(xhr)}`);
    });
});

/* When showing the modal, hide the "Delete" button if the modal is being used to create. */
$modal.on('show.bs.modal', function(event) {
    const isNewService = $(event.relatedTarget).hasClass("btn-add-service");
    $modal.find('form').find('.delete-service').toggle(!isNewService);
});


/* Parse the modal form into either a Service POST or PUT for the server. */
function serviceFormAsJson($form) {
    const findInput = (name) => $form.find(`input[name=${name}], select[name=${name}]`);
    const strInput = (name) => findInput(name).val();
    const intInput = (name) => parseInt(findInput(name).val(), 10);
    const floatInput = (name) => parseFloat(findInput(name).val(), 10);

    // Parse form data into JSON
    const data = {
        id: intInput("id"),
        name: strInput("name"),
        display_name: strInput("display_name"),
        description: strInput("description"),
        category: strInput("category"),
        points: floatInput("points"),
        script: strInput("script"),
        args: strInput("args"),
        last_octet: intInput("last_octet"),
        interval: intInput("interval"),
        min_rand: intInput("min_rand"),
        max_rand: intInput("max_rand"),
        disabled: findInput("disabled").prop("checked"),
        scorable: findInput("scorable").prop("checked"),
    };

    // Combine the date+time picker values together.
    // Browser handles conversion from local time picked by user -> to UTC.
    const starts_at_date = findInput("starts_at_date").val();
    const starts_at_time = findInput("starts_at_time").val();
    data.starts_at = new Date(`${starts_at_date}T${starts_at_time}`);

    return data;
}

/* Submit new/editted service */
$modal.find('form').on('submit', function saveService(event) {
    event.preventDefault();
    const $form = $(this);
    const data = serviceFormAsJson($form);

    const isNewService = data.id === -1;
    const now = new Date();
    data.modified_at = now;
    let gql_query = "";
    if(isNewService) {
        delete data.id;
        data.created_at = now;
        let mutation = `insert_service_one(object:{${window.C2GamesScoreboard.objToGqlStr(data)}}) {id}`;
        gql_query = `mutation insert { ${mutation} }`;
    } else {
        let service_id = data.id
        delete data.id;
        let mutation = `update_service(where: {id: {_eq: ${service_id}}}, _set:{${window.C2GamesScoreboard.objToGqlStr(data)}}) { affected_rows returning {id} }`;
        gql_query = `mutation update { ${mutation} }`;
    }
    C2GamesScoreboard.db.query(gql_query, {role: 'admin'}).then(function(ret) {
        console.log(`QGL ret: ${ret}`);
        alert("Page will reload.");
        window.location.reload();
    }).catch(e => {
        console.error(`error in services submit: ${e}`);
        alert(`Error occurred on submit: ${e}`);
    });
});

/* Delete Team */
$modal.find('form').on('click', '.delete-service', function deleteService(event) {
    const $form = $(event.delegateTarget);
    const id = $form.find("input[name=id]").val();
    const name = $form.find("input[name=name]").val();

    if(confirm(`Are you sure you want to delete "${name}"`)) {
        let mutation = `delete_service(where: {id: {_eq: ${id}}}) { affected_rows returning {id} }`
        C2GamesScoreboard.db.query(`mutation delete { ${mutation} }`, {role: 'admin'}).then(function(ret) {
            console.log(`QGL ret: ${ret}`);
            alert("Page will reload.");
            window.location.reload();
        }).catch(e => {
            console.error(`error in services submit: ${e}`);
            alert(`Error occurred on submit: ${e}`);
        });
    }
});


/* Add new service, button below the table */
$('.btn-add-service').on('click', function showTeamAddModal(event) {
    const $form = $modal.find('form');
    $form.trigger('reset');

    $modal.find('.modal-title').text("Add new team");
    $modal.find('input[name=id]').val("-1");

    // Modal show is wired up in the HTML, which then exposes the "relatedTarget"
    // on the event (part of bootstrap's API), which is used to hide/show a Delete button.
    //$modal.modal('show');
});

