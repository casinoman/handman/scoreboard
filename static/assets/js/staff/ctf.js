// DOM references for the global modal and inner flag submission form fields
const $modal = $('#ctf-edit-modal')
    , $editor  = $modal.find('textarea')
    , $preview = $modal.find('.preview')
    , $mkdnTabs= $modal.find(".nav-tabs a[data-toggle='tab']");

const $ctfConfig = $('.flag-config-table');

const getCtfRow = ($btn) => $btn.parentsUntil('tr').parent();

/* Display flag */
$ctfConfig.on('click', '.btn-flag', function displayFlag(event) {
    const $btn = $(event.currentTarget);
    const flag = $btn.attr('title');
    const name = getCtfRow($btn).children().eq(1).text();

    // show the flag with the text selected, ready to copy+paste
    window.prompt(`Flag for "${name}"`, flag);
});

/* Release a challenge */
$ctfConfig.on('click', '.btn-release', function releaseChallenge(event) {
    const $row = getCtfRow($(event.currentTarget));
    const challenge_id = $row.data('flag-id');
    const now = new Date();
    const data = {};

    data.modified_at = now;
    data.release_time = now;
    data.is_hidden = false;
    let mutation = `update_ctf_challenges(where: {id: {_eq: ${challenge_id}}}, _set:{${window.C2GamesScoreboard.objToGqlStr(data)}}){ affected_rows returning {id} }`
    C2GamesScoreboard.db.query(`mutation ctf_update { ${mutation} }`, {role: 'admin'}).then(function(ret) {
        console.log(`QGL ret: ${ret}`);
        alert("Page will reload.");
        window.location.reload();
    }).catch(e => {
        console.error(`error in ctf submit: ${e}`);
        alert(`Error occurred on submit: ${e}`);
    });
});

/* Show modal editor */
$ctfConfig.on('click', '.btn-edit', function showFlagEditorModal(event) {
    const $row = getCtfRow($(event.currentTarget));
    const flagID = $row.data('flag-id');

    // Very tight DOM coupling
    const $form = $modal.find('form');
    const $cells = $row.children();
    const findInput = (name) => $form.find(`input[name=${name}]`);
    const cellText = (idx) => $cells.eq(idx).text();

    // TODO: Make this better by not using index slices
    findInput("id").val(cellText(0));
    findInput("name").val(cellText(1));
    findInput("category").val(cellText(2));
    findInput("designer").val(cellText(3));
    findInput("points").val(cellText(4));

    findInput("flag").val($cells.eq(8).find('.btn-flag').attr('title'));
    $modal.find('.modal-title').text(`Edit ${cellText(1)}`);

    const isHidden = $cells.eq(5).children().length > 0;
    findInput("hidden").prop('checked', isHidden);

    // TODO: This fetches the entire challenge again, just
    // to get the description. This is a minor waste.
    $.getJSON(`/api/ctf/flags/${flagID}`).then(chal => {
        $editor.val(chal.description);
        $mkdnTabs.eq(0).tab('show');
    }, (xhr) => {
        $editor.val("");
        $mkdnTabs.eq(1).tab('show');

        $preview.html($(`<p class="text-danger" />`)
                       .text(`!! Couldn't fetch description: ${getXhrErr(xhr)}`));
    })
    .always(() => { $modal.modal('show'); });
});

/* When showing the modal, hide the "Delete" button if the modal is being used to create. */
$modal.on('show.bs.modal', function(event) {
    const isNewChallenge = $(event.relatedTarget).hasClass("btn-add-challenge");
    $modal.find('form').find('.delete-challenge').toggle(!isNewChallenge);
});

/* Live preview of markdown description */
$mkdnTabs.eq(1).on("show.bs.tab", function(event) {
    const desc = $editor.val();
    $preview.html(marked(desc));
});

/* Submit edited Challenge */
$modal.find('form').on('submit', function updateChallenge(event) {
    event.preventDefault();
    const $form = $(event.delegateTarget);
    const findInput = (name) => $form.find(`input[name=${name}]`);

    // Parse form data into JSON
    const data = {};
    data.description = $editor.val();
    data.is_hidden = findInput("hidden").prop("checked");
    ["id","name","category","designer","flag","points"].forEach(field => {
        data[field] = findInput(field).val();
    });
    data.id = parseInt(data.id);
    data.points = parseInt(data.points);

    const now = new Date();
    data.modified_at = now;
    const isNewChallenge = data.id === -1;
    if(isNewChallenge) {
        delete data.id;
        data.regex_enabled = false;
        data.team_specific = false;
        data.created_at = now;
        let mutation = `insert_ctf_challenges_one(object:{${window.C2GamesScoreboard.objToGqlStr(data)}}) {id}`
        C2GamesScoreboard.db.query(`mutation insert { ${mutation} }`, {role: 'admin'}).then(function(ret) {
            console.log(`QGL ret: ${ret}`);
            alert("Page will reload.");
            window.location.reload();
        }).catch(e => {
            console.error(`error in ctf submit: ${e}`);
            alert(`Error occurred on submit: ${e}`);
        });
    } else {
        let challenge_id = data.id
        delete data.id;
        let mutation = `update_ctf_challenges(where: {id: {_eq: ${challenge_id}}}, _set:{${window.C2GamesScoreboard.objToGqlStr(data)}}){ affected_rows returning {id} }`
        C2GamesScoreboard.db.query(`mutation update { ${mutation} }`, {role: 'admin'}).then(function(ret) {
            console.log(`QGL ret: ${ret}`);
            alert("Page will reload.");
            window.location.reload();
        }).catch(e => {
            console.error(`error in ctf submit: ${e}`);
            alert(`Error occurred on submit: ${e}`);
        });
    }
});

/* Add new challenge, button below the table */
$('.btn-add-challenge').on('click', function showChallengeAddModal(event) {
    const $form = $modal.find('form');
    $form.trigger('reset');
    $mkdnTabs.eq(0).tab('show');

    $modal.find('.modal-title').text("Add new challenge");
    $modal.find('input[name=id]').val("-1");

    // Modal show is wired up in the HTML, which then exposes the "relatedTarget"
    // on the event (part of bootstrap's API), which is used to hide/show a Delete button.
    //$modal.modal('show');
});

/* Delete challenge */
$modal.find('form').on('click', '.delete-challenge', function deleteChallenge(event) {
    const $form = $(event.delegateTarget);
    const flagID = $form.find("input[name=id]").val();
    const flagName = $form.find("input[name=name]").val();

    if(confirm(`Are you sure you want to delete "${flagName}"`)) {
        let mutation = `delete_ctf_challenges(where: {id: {_eq: ${flagID}}}) { affected_rows returning {id} }`
        C2GamesScoreboard.db.query(`mutation delete { ${mutation} }`, {role: 'admin'}).then(function(ret) {
            console.log(`QGL ret: ${ret}`);
            alert("Page will reload.");
            window.location.reload();
        }).catch(e => {
            console.error(`error in ctf submit: ${e}`);
            alert(`Error occurred on submit: ${e}`);
        });
    };
});


/* Upload Challenges via CSV */
const $csvUploadForm = $('.flag-config-csv-upload form');

(function initChallengeCsvEditor() {
    const placeholderFlagCsv = `
     Name,  Category, Designer, Hidden,                   Flag,                    Regex, Points, Description
 crypto-1,       CTF,      Bob,  false,   flag{1337_ch4ll3ng0},                         ,    100,  easy peasy
 crypto-2,       CTF,      Bob,   true,  flag{_!@*scr4mbl3r)(},                         ,    150,
   prog-1,      Prog,      Bob,  false, flag{why all the work},                         ,     50,
  Stage 1,      Wifi,      Dan,  false,          that was easy,  ^(?i)that\\s?was\\s?easy$,     25,
`.slice(1, -1); // trim the newlines at the start and end (but not the spaces)

    $csvUploadForm.find('textarea').val(placeholderFlagCsv);
})();

$csvUploadForm.on('submit', function addChallengesViaCSV(event) {
    event.preventDefault();
    const rawCSV = $csvUploadForm.find('textarea').val();

    let data;
    try {
        data = $.csv.toObjects(rawCSV, { onParseValue: e => e.trimLeft() });
    } catch (e) {
        alert(`CSV Upload failed: ${e.message}`)
        throw e;
    }

    // Lowercase all keys, cast values to scalars, rename fields as needed
    data = data.map(row => {
        row = Object.assign(...Object.entries(row).map(([k, v]) => ({[k.toLowerCase()]: v })));
        row.hidden = true;
        row.points = parseInt(row.points);
        row.regex_enabled = false;
        row.team_specific = false;
        row.created_at = new Date();
        delete row.id;
        return row;
    });


    let mutation = `insert_ctf_challenges(objects:{${window.C2GamesScoreboard.objToGqlStr(data)}}) { affected_rows returning {id} }`
    C2GamesScoreboard.db.query(`mutation ctf_insert_multiple { ${mutation} }`, {role: 'admin'}).then(function(ret) {
        console.log(`QGL ret: ${ret}`);
        alert("Page will reload.");
        window.location.reload();
    }).catch(e => {
        console.error(`error in ctf submit: ${e}`);
        alert(`Error occurred on submit: ${e}`);
    });
});
