#!/bin/bash

OUTDIR=$(dirname "$0")/ssl

openssl req -x509 -out $OUTDIR/localhost.crt -keyout $OUTDIR/localhost.key \
  -newkey rsa:2048 -nodes -sha256 \
  -extensions SAN -config <( \
   printf '
[req]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions = v3_req
distinguished_name = dn

[dn]
CN=localhost

[v3_req]
subjectAltName = @alt_names

[SAN]
subjectAltName = @alt_names

[alt_names]
DNS.1 = *.localhost
DNS.2 = localhost')